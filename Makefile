#Makefile at top of application tree
TOP = .
include $(TOP)/configure/CONFIG
DIRS += configure
DIRS += tlccsApp
#DIRS += vendor
#pico8App_DEPEND_DIRS += vendor

ifeq ($(BUILD_IOCS), YES)
DIRS += tlccsDemoApp
tlccsDemoApp_DEPEND_DIRS += tlccsApp
iocBoot_DEPEND_DIRS += tlccsDemoApp
DIRS += iocBoot
endif

include $(TOP)/configure/RULES_TOP
